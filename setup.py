from setuptools import setup, find_packages

setup(
    name='nyx',
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    zip_safe=False,
    entry_points={
        'console_scripts': ['nyx=nyx:main']
    },
    install_requires=[
        'appdirs>=1.4',
        'coloredlogs>=7.0',
        'fs>=2.0',
        'pluggy>=0.4',
        "psutil>=5.2",
        'py>=1.4',
        "six>=1.10",
        'toposort>=1.5',
    ],
    setup_requires=['pytest-runner', 'pytest-cov'],
    tests_require=['pytest', 'pyfakefs'],
    version='0.1.0',
    description='Universal builder of builders: for DevelOps',
    author='Joseph Benden',
    author_email='joe@benden.us',
    url='http://benden.us/',
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Quality Assurance",
        "Topic :: Software Development :: Testing",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: System :: Distributed Computing",
        "Topic :: Utilities"
    ]
)
