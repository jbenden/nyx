# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
The `environment` module handle the various aspects of ``Nyx`` program
initialization; such as deducing the active projects root location,
mapping in virtual file-systems, and walking the root for populating
the internal graph creation.
"""

from __future__ import absolute_import, division, print_function

import datetime
import logging
import os
import sys

import fs
from fs.multifs import MultiFS

from nyx.registry import Registry

is_win = (sys.platform == 'win32')                                     # pylint: disable=C0103

logger = logging.getLogger(__name__)                                   # pylint: disable=C0103


class WorkspaceNotFoundError(LookupError):
    """ Raised when a workspace cannot be determined from the working
    directory. """

    pass


def current_directory():
    """ Return an absolute, normalized path to the current working directory.

    :return: a normalized, absolute path for the current working directory.
    :rtype: str
    """

    return normalize_abspath(os.path.abspath(os.getcwd()))


def normalize_abspath(path):
    """ Return a normalized abspath from the given `path`. On Windows, this will ensure
    the drive letter is upper-case. On POSIX, the given `path` is returned unmodified.

    :arg str path:
        An absolute path to normalize
    """

    if (is_win and len(path) > 0 and                                   # pylint: disable=C1801
            path[0] in 'abcdefghijklmnopqrstuvwxyz'):
        return str(path[0]).upper() + path[1:]
    return path


def process_workspace_configuration(config):
    """ Process the users configuration to find the workspace root. Once the root has
    been determined, the remaining configuration KVPs are processed and all is
    stored into the ``Registry``.

    :arg config:
        The configuration
    :type config:
        NyxConfigParser
    :raises WorkspaceNotFoundError:
        if the workspace root directory cannot be determined.
    """

    projectdir = current_directory()
    Registry.environment.set('projectdir', projectdir)

    section = config.section_for_project_root(projectdir)
    if not section:
        raise WorkspaceNotFoundError('Could not find a workspace root.')

    name = section[len(config.workspace_section_prefix):]
    Registry.environment.set('name', name)

    root = config.get(section, 'root', fallback=None)
    Registry.environment.set('root', root)

    parts = [
        ('overlays', 'Overlays'),
        ('artifacts', 'Artifacts'),
    ]
    for part in parts:
        part_text = config.get(section, part[0], fallback=None)

        pieces = []
        if part_text:
            for overlay in part_text.splitlines():
                if os.path.isdir(overlay):
                    pieces.append(overlay)
                else:
                    logger.warning('The %s %s path does not exist, skipping.', part[0], overlay)

        Registry.environment.set(part[0], pieces)

    # log path
    logdir = config.get('global', 'logdir', fallback=None)
    if not logdir:
        logger.warning('Logging builds has been disabled. '
                       'Please see the logdir directive in the configuration file.')
        Registry.environment.set('logdir', None)
    else:
        logdir = os.path.join(logdir, name)
        Registry.environment.set('logdir', logdir)

        starttime = datetime.datetime.utcnow().strftime('%Y_%m_%d_%H_%M_%S')
        Registry.environment.set('log_subdir', starttime)

        log_full_dir = os.path.join(logdir, starttime)
        Registry.environment.set('log_full_dir', log_full_dir)
        logger.info('Builds will be logged to %s', log_full_dir)


def get_rootfs():
    """ Factory function to create and return a virtual file-system object,
    mounted at the workspace root directory.

    :return: the workspace root :py:mod:`fs` object
    :rtype: fs.base.FS
    :raises fs.errors.CreateFailed: if the workspace root does not exist

    .. warning::

        Do not forget to :py:meth:`fs.base.FS.close` the file-system or utilize
        the `with` syntax for automatic closing upon the scope closing.
    """

    return fs.open_fs(Registry.environment.get('root'))


def get_logfs():
    """ Factory function to create and return a virtual file-system object,
    mounted at the workspace's log directory. If no log directory was
    configured for the workspace, then `None` is returned.

    :return: the workspace's log directory :py:mod:`fs` object or None, if
        one was not configured
    :rtype: fs.base.FS or None
    :raises fs.errors.CreateFailed: if the workspace root does not exist

    .. warning::

        Do not forget to :py:meth:`fs.base.FS.close` the file-system or utilize
        the `with` syntax for automatic closing upon the scope closing.
    """

    if Registry.config.get('global', 'logdir', fallback=None) is not None:
        return fs.open_fs(Registry.environment.get('log_full_dir'))
    return None


def get_workspacefs():
    """ Factory function to create and return a virtual file-system object,
    presenting an entire view of the workspace; which includes all overlays
    mounted and finally the workspace root directory.

    :return: the workspace :py:mod:`fs` object
    :rtype: fs.base.FS or None
    :raises fs.errors.CreateFailed: if the workspace root does not exist

    .. warning::

        Do not forget to :py:meth:`fs.base.FS.close` the file-system or utilize
        the `with` syntax for automatic closing upon the scope closing.
    """

    workspacefs = MultiFS()
    for index, overlay in enumerate(Registry.environment.get('overlays')):
        workspacefs.add_fs('overlay_%d' % index, fs.open_fs(overlay))
    workspacefs.add_fs('workspace',
                       fs.open_fs(Registry.environment.get('root')),
                       write=True)
    return workspacefs


def get_artifactfs():
    """ Factory function to create and return a virtual file-system object,
    presenting an entire, merged view of the artifact directories associated
    with the workspace.

    :return: the artifact :py:mod:`fs` object
    :rtype: fs.base.FS or None
    :raises fs.errors.CreateFailed: if the workspace root does not exist

    .. warning::

        Do not forget to :py:meth:`fs.base.FS.close` the file-system or utilize
        the `with` syntax for automatic closing upon the scope closing.
    """

    artifactfs = MultiFS()

    n_artifacts = len(Registry.environment.get('artifacts')) - 1
    if n_artifacts < 0:
        return None

    for index, artifact in enumerate(Registry.environment.get('artifacts')):
        write = True if index == n_artifacts else False
        artifactfs.add_fs('artifact_%d' % index,
                          fs.open_fs(artifact),
                          write=write)
    return artifactfs


def _walker_on_error_handler(path, exc):                               # pragma: no coverage
    """ Handler for when the :py:func:`fs.walk` throws an exception. """

    logger.warning('An error occurred while scanning; path %s threw %r', path, exc)
    return True


def process_workspace_projects(callback):
    """ Searches the entire workspace virtual file-system for all ``Nyx`` project
    files. For each project file found, the passed `callback` parameter will be
    invoked with the virtual path to the file as the first argument.

    :param callable callback:
        this callable is invoked for each found ``Nyx`` project file, with a virtual path
        to the project file as the first argument.
    """

    with get_workspacefs() as workspacefs:
        for path in workspacefs.walk.files(filter=['PROJECT.nyx', 'PROJECT.py'],
                                           exclude_dirs=['.git', '.hg', '.svn', '.bzr'],
                                           on_error=_walker_on_error_handler):
            logger.debug('Found %s project file.', path)
            callback(path)
