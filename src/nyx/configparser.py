# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0111,R0901,R1705

from __future__ import absolute_import, division, print_function

import configparser
import locale
import io
import os
import sys

from appdirs import site_config_dir, user_config_dir
from nyx import environment


class CascadingConfigParser(configparser.ConfigParser):
    """
    ``CascadingConfigParser`` permits reading and writing INI-style configurations
    from multiple sources, which may be dict-like data, strings, or files.
    Additionally, ``CascadingConfigParser`` handles Python versioning mismatches,
    default configuration, and configurations that may arrive through the
    environment of the process.
    """

    def __init__(self):
        configparser.ConfigParser.__init__(self,
                                           default_section='global',
                                           interpolation=configparser.ExtendedInterpolation())
        # enable case-sensitive INI sections
        self.optionxform = str

    def set_defaults(self, defaults):
        """
        Set default configuration data from any object that provides a dict-like
        :func:`items()` method. Keys are section names, values are dictionaries with keys and
        values that should be present in the section. If the used dictionary type
        preserves order, sections and their keys will be added in order. All values
        are automatically converted to strings.
        """

        self.read_dict(defaults)

    def read(self, filenames, encoding='utf-8'):
        """
        Attempt to read and parse a list of filenames, returning a list of filenames which
        were successfully parsed.
        """

        if sys.version_info[0] == 3 and sys.version_info[1] >= 2:
            return super(CascadingConfigParser, self).read(filenames, encoding=encoding)
        else:                                                          # pragma: no cover
            results = []
            for filename in filenames:
                try:
                    with open(filename, 'rb') as f:  # pylint: disable=C0103
                        siof = io.StringIO(initial_value=f.read().decode(encoding))
                        self.read_file(siof, source=filename)
                        results.append(filename)
                except IOError:
                    pass
            return results

    def read_env(self, prefix=None, encoding='utf-8'):
        """
        Attempt to read and parse environment variables prefixed by `prefix`. All
        environment variables are converted to lower-case; however, values are not.

        :arg prefix: a string by which all processable environment variables will
            contain at the start of their variable name.
        :arg encoding: the encoding of all environment variables' names and values.
            Defaults to `UTF-8` encoding.

        :returns: nothing.

        Example usage:

        If the following environment variable is defined ::

                NYX_GENERAL_DEBUG=yes

        The following INI-file snippet is thus equivalent ::

                [general]
                debug=yes

        And the ``CascadingConfigParser`` will thus be written and produce ::

                parser = CascadingConfigParser()
                parser.read_env('NYX')
                value = parser.get('general', 'debug')
                assert value == 'yes'
        """

        for key, value in os.environ.items():
            if key.startswith(prefix):
                keys = key[len(prefix) + 1:].split('_')
                if keys and len(keys) % 2 != 0:
                    continue
                elif keys:
                    section = {                                        # pragma: no coverage
                        type(''): lambda x: x.decode(encoding),        # Python 2
                        str: lambda x: x,                              # Python 3
                    }.get(type(keys[0]), str)(keys[0]).lower()

                    option = {                                         # pragma: no coverage
                        type(''): lambda x: x.decode(encoding),        # Python 2
                        str: lambda x: x,                              # Python 3
                    }.get(type(keys[1]), str)(keys[1]).lower()

                    self.set(section, option, {                        # pragma: no coverage
                        type(''): lambda x: x.decode(encoding),        # Python 2
                        str: lambda x: x,                              # Python 3
                    }.get(type(value), str)(value))


class NyxConfigParser(CascadingConfigParser):
    """
    A ``NyxConfigParser`` handles all aspects of the ``Nyx`` cascading configuration.
    """

    def __init__(self):
        """
        Initializer for ``NyxConfigParser`` sets up the configuration
        defaults and imports all environment variables under the
        ``env`` INI section.
        """

        CascadingConfigParser.__init__(self)

        encoding = locale.getpreferredencoding(do_setlocale=True)

        # set defaults
        self.set_defaults({
            'global': {
                'debug': 'off'
            }
        })

        # import all environment variables
        self.add_section('env')
        for key, value in os.environ.items():
            key = {                                                     # pragma: no coverage
                type(''): lambda x: x.decode(encoding=encoding),        # Python 2
                str: lambda x: x,                                       # Python 3
            }.get(type(key), str)(key).lower()

            value = {                                                   # pragma: no coverage
                type(''): lambda x: x.decode(encoding=encoding),        # Python 2
                str: lambda x: x,                                       # Python 3
            }.get(type(value), str)(value).lower()

            self.set('env', key, value)

    def read_all(self):
        """ Read all possible configurations; both from files and the environment. """

        encoding = locale.getpreferredencoding(do_setlocale=False)

        # read available configs; from least-to-most specific
        appname = 'Nyx'

        site_conf = os.path.join(site_config_dir(appname), 'nyx.conf')
        user_conf = os.path.join(user_config_dir(appname), 'nyx.conf')
        userhome_conf = os.path.expanduser('~/.nyx.conf')
        self.read([site_conf, user_conf, userhome_conf], encoding=encoding)

        # process our environment specific configuration overrides
        self.read_env(prefix='NYX')

    workspace_section_prefix = 'root:'

    def section_for_project_root(self, path):
        """ Determine the INI section which correlates to the workspace
        root of the `path` given.

        :arg str path:
            an absolute path to match against all known workspaces configured
        :return: an INI section name that matched,
            or `None` if unable to locate a matching workspace
        :rtype: str or None
        """

        path = environment.normalize_abspath(os.path.abspath(path))

        for section in list(filter(lambda x: x.startswith(self.workspace_section_prefix),
                                   self.sections())):
            # does this root match our working environment?
            root = self.get(section, 'root', fallback=None)
            if root is None or len(root) == 0:                         # pylint: disable=C1801
                continue
            root = environment.normalize_abspath(root)

            # is `path` contained within `root`?
            if path.startswith(root):
                return section

        return None
