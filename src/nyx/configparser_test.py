# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0103,C0111,W0621

from __future__ import absolute_import, division, print_function

import pytest

from nyx.configparser import CascadingConfigParser, NyxConfigParser


@pytest.fixture(scope='function')
def parser():
    yield CascadingConfigParser()


def test_simple_config(parser):
    config = u"""
[global]
debug = yes
"""
    parser.set_defaults({'global': {'debug': 'no'}})
    assert parser['global']['debug'] == 'no'

    parser.read_string(config)
    assert parser['global']['debug'] == 'yes'


def test_envars_override(monkeypatch, parser):
    parser.set_defaults({'global': {'debug': 'no'}})
    assert parser['global']['debug'] == 'no'

    monkeypatch.setenv('NYX_GLOBAL_DEBUG', 'yes')
    parser.read_env(prefix='NYX')
    assert parser['global']['debug'] == 'yes'


def test_improper_envars_override(monkeypatch, parser):
    parser.set_defaults({'global': {'debug': 'no'}})
    assert parser['global']['debug'] == 'no'

    monkeypatch.setenv('NYX_DEBUG', 'yes')
    parser.read_env(prefix='NYX')
    assert parser['global']['debug'] == 'no'


def test_blank_nyx_config(monkeypatch):
    monkeypatch.setenv('NYX_GLOBAL_DEBUG', 'yes')
    parser = NyxConfigParser()
    parser.read_all()
    assert parser['global']['debug'] == 'yes'


def test_nyx_determine_project_root():
    config = u"""
[global]
debug = yes

[root:Main Sources]
root = c:\\Source

[root:Broken-Blank]
root =

[root:Broken-Missing]
logs = C:\\Logs

[root:Go Sources]
root = c:\\Users\\Joseph Benden\\go
"""
    parser = NyxConfigParser()
    parser.read_string(config)

    assert parser.section_for_project_root('C:\\Windows') is None
    assert parser.section_for_project_root('C:\\Source') == 'root:Main Sources'
    assert parser.section_for_project_root('c:\\Users\\Joseph Benden\\go') == 'root:Go Sources'
