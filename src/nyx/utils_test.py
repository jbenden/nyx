# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0103,C0111,W0621,R0903

from __future__ import absolute_import, division, print_function

import six

from nyx.utils import Singleton


@six.add_metaclass(Singleton)
class DummySingleton(object):
    __metaclass__ = Singleton

    var = 42


def test_singleton():
    obj1 = DummySingleton()
    obj2 = DummySingleton()
    assert obj1 == obj2
    assert obj1.var == obj2.var
