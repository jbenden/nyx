# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=W0611,C0111,wildcard-import,unused-wildcard-import

import sys

import pytest

from nyx.test_utils import *


def pytest_addoption(parser):
    parser.addoption('--runslow', action='store_true',
                     help='run slow tests')


def pytest_configure(config):                                          # pylint: disable=W0613
    if sys.version_info < (3, 0):                                      # pragma: no coverage
        import codecs
        codecs.register(lambda name: codecs.lookup('utf-8') if name == 'cp65001' else None)
