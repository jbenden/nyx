# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Various models used across process-boundaries through the
``multiprocessing`` module.
"""

from __future__ import absolute_import, division, print_function

import json


class Environment(object):
    """ Holder of all ``Nyx`` run-time variables. """

    _databag = {}

    def dump(self):
        """
        Outputs all ``Environment`` variables using the :func:`print()` function.
        """

        return json.JSONEncoder().encode(self._databag)                      # pragma: no coverage

    def get(self, name):
        """ Retrieve the environment variable `name`. """

        return self._databag.get(name)                                       # pragma: no coverage

    def set(self, name, value):
        """ Write the contents of `value` to the environment variable named `name`. """

        self._databag.update([(name, value)])                                # pragma: no coverage


class Targets(object):
    """ Holder of all ``Nyx`` targets read from the available project files. """

    _targets = {}

    def get(self, name):
        """ Retrieve the target named `name`. """

        return self._targets.get(name)                                       # pragma: no coverage

    def set(self, name, value):
        """ Write the contents of `value` to the target named `name`. """

        self._targets.update([(name, value)])                                # pragma: no coverage
