# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0103,C0111,W0621,R0801,R0903,unused-argument,unused-variable

from __future__ import absolute_import, division, print_function

from contextlib import contextmanager
import fs
from pyfakefs import fake_filesystem
import pytest

from nyx import environment
from nyx.configparser import NyxConfigParser
from nyx import models
from nyx.registry import Registry


@pytest.fixture
def indir(monkeypatch, tmpdir):
    monkeypatch.chdir(tmpdir)
    yield tmpdir


def test_current_directory(indir, fs):
    fs.CreateDirectory(str(indir))

    my_os_module = fake_filesystem.FakeOsModule(fs)
    my_os_module.chdir(str(indir))
    assert indir == environment.current_directory()


def test_normalize_abspath():
    assert environment.normalize_abspath('c:\\') != 'c:\\'
    assert environment.normalize_abspath('C:\\') == 'C:\\'


@pytest.fixture
def workspace(indir, fs):
    with workspace_tester(indir, fs) as ws:
        yield ws


@contextmanager
def workspace_tester(indir, fs, config=None):
    fs.CreateDirectory(str(indir))

    my_os_module = fake_filesystem.FakeOsModule(fs)
    my_os_module.chdir(str(indir))
    assert indir == environment.current_directory()

    # create a working artifact path inside of the test file-system
    fs.CreateDirectory(my_os_module.path.join(str(indir), 'Artifacts'))

    # create a working overlay path inside of the test file-system
    overlaydir = my_os_module.path.join(str(indir), 'Overlays')
    fs.CreateDirectory(overlaydir)
    fs.CreateFile(my_os_module.path.join(overlaydir, 'a.txt'), contents='overlay file')
    fs.CreateFile(my_os_module.path.join(str(indir), 'a.txt'), contents='workspace file')
    fs.CreateFile(my_os_module.path.join(overlaydir, 'b.txt'), contents='overlay file')
    fs.CreateFile(my_os_module.path.join(str(indir), 'c.txt'), contents='workspace file')

    # take the normalized version, incase of normalization discrepancies.
    indir = environment.current_directory()

    if config is None:
        config = u"""
[global]
debug = yes
datadir = C:\\Builds
logdir = ${{global:datadir}}\\Logs

[root:Test]
root = {0}
artifacts = {0}\\Artifacts
overlays = {0}\\Overlays
"""
    config = config.format(str(indir))

    parser = NyxConfigParser()
    parser.read_string(config)

    Registry.config = parser
    Registry.environment = models.Environment()
    Registry.targets = models.Targets()

    environment.process_workspace_configuration(parser)

    log_full_dir = Registry.environment.get('log_full_dir')
    fs.CreateDirectory(log_full_dir)

    yield str(indir)


def test_workspace_configuration(workspace):
    assert Registry.environment.get('root') == workspace
    assert Registry.environment.get('projectdir') == workspace
    assert Registry.environment.get('name') == 'Test'
    assert Registry.environment.get('logdir') == 'C:\\Builds\\Logs\\Test'


def test_rootfs(workspace):
    assert isinstance(environment.get_rootfs(), fs.base.FS)


def test_logfs(workspace):
    assert isinstance(environment.get_logfs(), fs.base.FS)


def test_workspacefs(workspace):
    wrkfs = environment.get_workspacefs()
    assert isinstance(wrkfs, fs.base.FS)

    with wrkfs.open('a.txt') as f:
        content = f.read()
    assert content == 'workspace file'

    content = ''
    with wrkfs.open('b.txt') as f:
        content = f.read()
    assert content == 'overlay file'

    content = ''
    with wrkfs.open('c.txt') as f:
        content = f.read()
    assert content == 'workspace file'


def test_artifactfs(workspace):
    assert isinstance(environment.get_artifactfs(), fs.base.FS)


def test_missing_section(indir, fs):
    config = u"""
[global]
debug = yes

[root:Test]
root = {0}a
"""
    with pytest.raises(environment.WorkspaceNotFoundError):
        with workspace_tester(indir, fs, config) as ws:
            assert Registry.environment.get('root') is None


def test_missing_root(indir, fs):
    config = u"""
[global]
debug = yes

[root:Test]
"""
    with pytest.raises(environment.WorkspaceNotFoundError):
        with workspace_tester(indir, fs, config) as ws:
            assert Registry.environment.get('root') is None


def test_missing_log_directory(indir, fs):
    config = u"""
[global]
debug = yes
datadir = C:\\Builds

[root:Test]
root = {0}
"""
    with workspace_tester(indir, fs, config) as ws:
        assert Registry.environment.get('logdir') is None
        assert environment.get_logfs() is None


def test_missing_artifact_directory(indir, fs):
    config = u"""
[global]
debug = yes
datadir = C:\\Builds
logdir = ${{global:datadir}}\\Logs

[root:Test]
root = {0}
artifacts = {0}\\Artifacts-ReadOnly
    {0}\\Artifacts
overlays = {0}\\Overlays
"""
    with workspace_tester(indir, fs, config) as ws:
        assert len(Registry.environment.get('artifacts')) == 1


def test_missing_artifacts(indir, fs):
    config = u"""
[global]
debug = yes
datadir = C:\\Builds

[root:Test]
root = {0}
"""
    with workspace_tester(indir, fs, config) as ws:
        assert environment.get_artifactfs() is None


def test_missing_overlay_directory(indir, fs):
    config = u"""
[global]
debug = yes
datadir = C:\\Builds
logdir = ${{global:datadir}}\\Logs

[root:Test]
root = {0}
artifacts = {0}\\Artifacts-ReadOnly
    {0}\\Artifacts
overlays = {0}\\Overlays-ReadOnly
    {0}\\Overlays
"""
    with workspace_tester(indir, fs, config) as ws:
        assert len(Registry.environment.get('overlays')) == 1


def test_workspace_projects(wrkspacetmp):
    found = {'count': 0}

    def _found_project_file(path):
        found['count'] += 1

    environment.process_workspace_projects(_found_project_file)
    assert found['count'] == 2
