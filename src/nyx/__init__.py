# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
nyx: a builder of builders

nyx (pronunciation “/ˈnɪks/”)

From Wikipedia: Nyx stood at or near the beginning of creation and mothered other
personified deities.

Nyx stands above all other software projects you may have, providing support
tools for managing the lifecycles around all of them; such as:

* correctly building software and its' software dependencies.
* correctly building all dependent software, upon a dependency change.
* analyzing software in new ways, using graph techniques.
"""

# pylint: disable=C0103

from __future__ import absolute_import, division, print_function

import codecs
from contextlib import contextmanager
import argparse
import sys
import os
import logging
from multiprocessing import cpu_count, Manager, Pool, freeze_support, log_to_stderr, SUBWARNING
import signal
import time

import coloredlogs
import pluggy
import six

from nyx import hookspecs
from nyx.configparser import NyxConfigParser
from nyx import environment as nyx_environment
from nyx import multiprocessing as nyx_mp
from nyx.registry import Registry

hookimpl = pluggy.HookimplMarker("nyx")

mlogger = log_to_stderr()
mlogger.setLevel(SUBWARNING)
logger = logging.getLogger()

try:
    CPUS_ONLINE = cpu_count()
except NotImplementedError:
    # The multiprocessing module sometimes is unable to report the number of cpus
    # available. This is typically on Windows, when an environment variable
    # is used to gather the information, and it is missing... Work around this
    # problem by using a different method to acquire the number of CPUs
    # online and available.
    def _cpu_count():
        import psutil
        return psutil.cpu_count()
    CPUS_ONLINE = _cpu_count()

STAGE_NAME = {
    0: "loading",   # Load available project files, based on passed cmdline args
    1: "analysis",  # Analyze for missing dependencies and topologically sort them
    2: "execution"  # Execute all build steps to produce artifacts
}


class LoggerMap(object):                       # pylint: disable=C0111,R0903
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    NOTIFY = (logging.INFO + logging.WARN) // 2
    WARN = WARNING = logging.WARN
    ERROR = logging.ERROR
    FATAL = logging.FATAL

    # LEVELS = [DEBUG, INFO, NOTIFY, WARN, ERROR, FATAL]
    LEVELS = [DEBUG, INFO, WARN, ERROR, FATAL]

    @classmethod
    def level_for_integer(cls, level):         # pylint: disable=C0111
        levels = cls.LEVELS
        if level < 0:
            return levels[0]
        if level >= len(levels):
            return levels[-1]
        return levels[level]


def _main():
    """ Main application entry point. """

    # handle command-line arguments passed in
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='subcommands',
                                       metavar='',
                                       dest='subparser_name')

    parser_build = subparsers.add_parser('build', help='Build one or more targets')
    parser_build.add_argument(
        '-a', '--arch',
        dest='arch',
        default='amd64',
        help='The architecture to produce artifacts for (default: amd64)')
    parser_build.add_argument(
        '-t', '--type',
        dest='type',
        default='release',
        help='The build type to produce (eg: release, debug)')
    parser_build.add_argument(
        'targets',
        nargs='+',
        type=str,
        help='target(s) to build')
    # parser_build.set_defaults(func=exec_targets)

    parser.add_argument(
        '-v', '--verbose',
        action='count',
        dest='verbose',
        default=0,
        help='Increase output verbosity.')

    parser.add_argument(
        '-q', '--quiet',
        action='count',
        dest='quiet',
        default=0,
        help='Decrease output verbosity.')

    parser.add_argument(
        '-n', '--dry-run',
        dest='dry_run',
        action='store_true',
        help='Show what would happen, without making any changes.')

    Registry.plugin_manager.hook.nyx_addoption(parser=parser)

    try:
        options = parser.parse_args()
    except TypeError:
        parser.print_help()
        sys.exit(2)

    global logger                              # pylint: disable=W0603

    verbosity = options.verbose - options.quiet
    logger.setLevel(LoggerMap.level_for_integer(2 - verbosity))
    logger.propagate = False

    if not options.subparser_name:
        print('A subcommand was not given.')
        parser.print_help()
        sys.exit(2)

    logger.debug('Starting up....')


def mgr_init():
    """ Initializer for NyxManager """

    signal.signal(signal.SIGINT, signal.SIG_IGN)


def _get_plugin_manager(config, plugins=()):
    pm = pluggy.PluginManager('nyx')
    pm.add_hookspecs(hookspecs)
    pm.set_blocked('nyx_mp')
    pm.register(sys.modules[__name__])
    pm.load_setuptools_entrypoints('nyx')
    for plugin in plugins:
        if isinstance(plugin, six.string_types):
            __import__(plugin)
            try:
                pm.register(sys.modules[plugin])
            except KeyError:
                print('The %s plugin was requested to load and register; but failed to import!' % plugin, file=sys.stderr)  # pylint: disable=C0301
                sys.exit(1)
        else:
            pm.register(plugin)
    pm.check_pending()

    if config.getboolean('global', 'debug', fallback=False):
        # magic line to set a writer function
        pm.trace.root.setwriter(print)
        pm.enable_tracing()

    return pm


@hookimpl(tryfirst=True)
def nyx_early_configure(config):
    """ Decipher the working root location and initialize the environment. """

    nyx_environment.process_workspace_configuration(config)

    def _found_prj_file(path):
        logger.debug(path)
    nyx_environment.process_workspace_projects(_found_prj_file)


@hookimpl(trylast=True)
def nyx_setup_logging(config, filename):                           # pylint: disable=W0613
    """ Configures the root logger instance. """

    # gather logging configuration
    log_level = logging.DEBUG
    log_format = config.get('global', 'log_format',
                            fallback='%(asctime)s,%(msecs)03d %(levelname)s:%(name)s %(message)s',
                            raw=True)
    log_date_format = config.get('global', 'log_date_format',
                                 fallback='%Y-%m-%d %H:%M:%S',
                                 raw=True)

    # initialize the root logging subsystem
    coloredlogs.install(fmt=log_format, datefmt=log_date_format,
                        level=logging.getLevelName(log_level))

    # inform pluggy we set up logging
    return logging.getLogger()


def _pool_init(config, environment, filename, lock, plugins, targets):  # pylint: disable=R0913
    Registry.lock = lock
    Registry.config = config
    Registry.environment = environment
    Registry.targets = targets

    Registry.plugin_manager = _get_plugin_manager(config, plugins=plugins)

    Registry.plugin_manager.hook.nyx_setup_logging(config=config, filename=filename)

    logger.debug('_pool_init entered')


def f():                                       # pylint: disable=C0111

    logger.warning('Hello world at %d!', Registry.environment.get('stage'))
    Registry.environment.set('stage', 144)

    logger.warning('Config says %s', Registry.config.get('global', 'debug'))


def main():
    """ Main application entry point. """

    # support frozen multiprocessing module
    freeze_support()

    # monkey-patch Windows UTF-8 consoles; Python 2.7 doesn't handle cp65001.
    if sys.version_info[:2] < (3, 0):
        codecs.register(lambda name: codecs.lookup('utf-8') if name == 'cp65001' else None)

    with nyx_multiprocessing() as pool:
        time.sleep(5)
        Registry.environment.set('stage', 42)

        pool.apply(f)

        logger.debug('stage is now %d', Registry.environment.get('stage'))

        _main()


@contextmanager
def nyx_multiprocessing(plugins=(), filename=None, nproc=CPUS_ONLINE, do_early_configure=True):
    """ A generator function, for use in the ``with`` statement, for encapsulating
    a block of code inside the scope of a fully initialized and working ``Nyx``
    ``multiprocessing`` context.
    """

    # ensure all multiprocessing registrations occurred
    nyx_mp.register()

    # read our configuration
    config = NyxConfigParser()
    config.read_all()

    # determine number of processes to utilize
    nproc = config.get('global', 'nproc', fallback=nproc)

    Registry.config = config
    assert Registry.config == config

    Registry.plugin_manager = _get_plugin_manager(config, plugins=plugins)
    assert isinstance(Registry.plugin_manager, pluggy.PluginManager)

    # initialize the root logging subsystem
    Registry.plugin_manager.hook.nyx_setup_logging(config=config, filename=filename)

    # launch our multiprocessing manager instance
    manager = nyx_mp.NyxManager()
    manager.start(mgr_init)

    try:
        lock = manager.RLock()
        environment = manager.environment()    # pylint: disable=E1101
        targets = manager.targets()            # pylint: disable=E1101

        Registry.lock = lock
        Registry.environment = environment
        Registry.targets = targets

        assert Registry.lock == lock
        assert Registry.environment == environment
        assert Registry.targets == targets

        if do_early_configure:
            # allow plugins to configure themselves
            Registry.plugin_manager.hook.nyx_early_configure(config=config)

        # allow plugins to configure themselves
        Registry.plugin_manager.hook.nyx_configure(config=config)

        pool = Pool(processes=nproc,
                    initializer=_pool_init,
                    initargs=[config, environment, filename, lock, plugins, targets])

        try:
            # permit caller to perform tasks!
            yield pool
        except KeyboardInterrupt:
            logger.warning('Got Ctrl-C. Stopping everything, please wait...')
            pool.close()
            pool.terminate()
        else:
            pool.close()
            pool.join()
    finally:
        logger.debug('NyxManager is shutting down...')
        manager.shutdown()
