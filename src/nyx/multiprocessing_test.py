# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0103,C0111,W0621,R0801,R0903

from __future__ import absolute_import, division, print_function

import logging

from nyx import hookimpl, nyx_multiprocessing
from nyx.registry import Registry
from nyx.test_utils import slow

logger = logging.getLogger()


@hookimpl(tryfirst=True)
def nyx_setup_logging(config, filename):                               # pylint: disable=W0613
    global logger                                                      # pylint: disable=W0603

    logging.basicConfig(format='%(asctime)s %(levelname)s:%(filename)s:%(lineno)s %(message)s',
                        level=logging.DEBUG)

    # set up child logging to the logs folder for the target.
    chl = logging.FileHandler(filename, encoding='utf-8')
    chl.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s:%(name)s %(message)s')
    chl.setFormatter(formatter)
    logger.addHandler(chl)
    logger.propagate = True

    logging.root.handlers = []
    logging.root.addHandler(chl)

    logger.debug('Logging to file %s', filename)
    return logging.getLogger()


def myf():                                                             # pylint: disable=C0111
    Registry.environment.set('stage', 144)
    Registry.targets.set('//test:all', 'world')


@slow
def test_multiprocessing(tmpdir):
    args = {
        'nproc': 1,
        'filename': str(tmpdir.join("test.log")),
        'plugins': (__name__,),
        'do_early_configure': False,
    }
    with nyx_multiprocessing(**args) as pool:
        Registry.environment.set('stage', 42)
        assert Registry.environment.get('stage') == 42

        Registry.targets.set('//test:all', 'hello')
        assert Registry.targets.get('//test:all') == 'hello'

        pool.apply(myf)

        assert Registry.environment.get('stage') == 144
        assert Registry.targets.get('//test:all') == 'world'

        logger.debug('Registry.environment.dump: %r', Registry.environment.dump())

    found = False
    for line in tmpdir.join('test.log').readlines():
        if line.find('dump: ') != -1:
            found = True
    assert found is True
