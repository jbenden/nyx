# -*- coding: utf-8 -*-
# Copyright 2017 Joseph Benden <joe@benden.us>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=C0103,C0111,W0621,R0801,R0903

from __future__ import absolute_import, division, print_function

from contextlib import contextmanager
import os
import shutil

import pytest

from nyx import environment
from nyx.configparser import NyxConfigParser
from nyx import models
from nyx.registry import Registry

fixture_dir = os.path.join(os.path.dirname(__file__), 'fixtures')

try:
    slow = pytest.mark.skipif(
        not pytest.config.getoption('--runslow'),                      # pylint: disable=E1101
        reason='need --runslow option to run'
    )
except AttributeError:
    def slow(*args, **kwargs):                                         # pylint: disable=W0613
        return args[0]


@contextmanager
def nyx_workspace_fixture(indir, config=None):
    # take the normalized version, incase of normalization discrepancies.
    indir = environment.current_directory()

    if config is None:
        config = u"""
[global]
debug = yes
logdir = {0}\\Logs

[root:Test]
root = {0}
artifacts = {0}\\Artifacts
overlays = {0}\\Overlays
"""
    config = config.format(str(indir))

    parser = NyxConfigParser()
    parser.read_string(config)

    Registry.config = parser
    Registry.environment = models.Environment()
    Registry.targets = models.Targets()

    environment.process_workspace_configuration(parser)

    yield str(indir)


@pytest.fixture(name='wrkspacetmp')
def fixture_wrkspacetmp(project):
    with nyx_workspace_fixture(project) as ws:
        yield ws


@pytest.fixture(name='project')
def fixture_project(monkeypatch, request, tmpdir):
    dest = os.path.join(str(tmpdir), 'root')
    fixture_dirs = [
        request.function.__name__,
        getattr(request.module, '__fixtures__', None),
        request.module.__name__.replace('.', '_'),
    ]
    for path in fixture_dirs:
        if path is not None:
            where = os.path.join(fixture_dir, path)
            if os.path.exists(where):
                shutil.copytree(where, dest)

    monkeypatch.chdir(dest)
    yield dest
