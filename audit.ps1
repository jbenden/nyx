$python = ".\Scripts\python.exe"
$rst2html = ".\Scripts\rst2html5.py"
$pep8 = ".\Scripts\pep8.exe"

Write-Output ">>> Run unit tests, investigate code coverage ..."
& ".\Scripts\pytest.exe" --cov-report term --cov-report html --cov --runslow src
Write-Output ">>> Run setup.py check"
& "$python" setup.py check
Write-Output ">>> Run python setup.py --long-description | rst2html.py ..."
& "$python" setup.py --long-description | & "$python" "$rst2html" | Out-Null
Write-Output ">>> Run rst2html README.rst"
& "$python" "$rst2html" .\README.rst .\README.html
Write-Output ">>> Run PEP8 check ..."
& ".\Scripts\pep8.exe" src
Write-Output ">>> Run pylint on nyx ..."
& ".\Scripts\pylint.exe" --reports=n src
