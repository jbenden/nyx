Mythology
=========

A shadowy figure, Nyx_ stood at or near the beginning of creation and mothered other
personified deities [WIKIPEDIA]_.


Overview
========

Nyx (pronunciation “/ˈnɪks/”) stands above all software projects one may have,
manage, or depend upon; providing tooling for managing the full life-cycle
around them all.

Nyx benefits include:

    - correctly building software and its' software dependencies.
    - correctly building all dependent software, upon a dependency change.
    - analyzing software in new ways, using graph techniques.


Contact & Help
==============
Your feedback, questions, and comments are very much welcomed. Please contact me
via email at joe@benden.us or use the `project's issue tracker
<https://gitlab.com/jbenden/nyx/issues>`_ hosted on GitLab.com.


Author & License
================
Nyx is written and maintained by `Joseph Benden <http://benden.us>`_.

Nyx is licensed under an Apache 2.0 license (see ``LICENSE.txt`` file; or alternatively
view the `Apache License 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_ online).

.. [WIKIPEDIA] Nyx. (2017, May 14). In Wikipedia, The Free Encyclopedia. Retrieved
   23:36, June 2, 2017, from `https://en.wikipedia.org/w/index.php?title=Nyx&oldid=780302548 <https://en.wikipedia.org/w/index.php?title=Nyx&oldid=780302548>`_.
.. _Nyx: https://en.wikipedia.org/wiki/Nyx
