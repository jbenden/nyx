#!/bin/bash

echo ">>> Run unit tests, investigate code coverage ..."
pytest --cov-report term --cov-report html --cov --runslow src
echo ">>> Run setup.py check"
python setup.py check
echo ">>> Run python setup.py --long-description | rst2html.py ..."
python setup.py --long-description | rst2html.py > /dev/null
echo ">>> Run rst2html README.rst"
rst2html.py README.rst README.html
echo ">>> Run PEP8 check ..."
pep8 src
echo ">>> Run pylint on nyx ..."
pylint --reports=n src
